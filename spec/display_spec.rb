# frozen_string_literal: true

require_relative '../lib/presenter/display'

RSpec.describe Presenter::Display do
  let(:headers) { %w[page visits] }
  let(:rows) { [{ page: '/about/2', visits: 90 }, { page: '/contact', visits: 89 }] }
  let(:line_width) { 25 }

  it 'generates headers' do
    expect(described_class.send(:generate_headers, headers)).to eq("#{'page'.ljust(line_width)}visits\n")
  end

  it 'generates rows' do
    expect(described_class.send(:generate_rows, headers,
                                rows)).to eq("#{'/about/2'.ljust(line_width)}90\n#{'/contact'.ljust(line_width)}89\n")
  end
end
