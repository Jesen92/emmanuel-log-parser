# frozen_string_literal: true

require_relative '../lib/parser/file'
require_relative '../lib/error/invalid_input'

RSpec.describe Parser::File do
  let(:valid_file) { File.open('spec/fixtures/webserver.log', 'r') }
  let(:invalid_file) { File.open('spec/fixtures/webserver_invalid.log', 'r') }
  subject { described_class.new(valid_file).call }

  context 'parsing from files to hash of pages' do
    it 'content is valid' do
      expect(subject).to eq(
        {
          '/help_page/1' => ['126.318.035.038', '929.398.951.889'],
          '/contact' => ['184.123.665.067'],
          '/home' => ['184.123.665.067'],
          '/about/2' => ['444.701.448.104']
        }
      )
    end

    it 'raises error when page or view is missing in a line' do
      expect {
        Parser::File.new(invalid_file).call
      }.to raise_error(Error::InvalidInput)
    end

    it 'returns correct error message' do
      expect {
        Parser::File.new(invalid_file).call
      }.to raise_error('page or view input missing on line 5')
    end
  end
end
