# frozen_string_literal: true

require_relative '../lib/parser/page'
require_relative '../lib/error/invalid_input'

RSpec.describe Parser::Page do
  subject { described_class.new({ page: '/index', view: '126.318.035.038', line: 1 }) }
  let(:valid_ip_instance) { described_class.new({ page: '/index', view: '127.0.0.1', line: 2 }) }

  context 'validating page data' do
    it 'raises error on an invalid IP' do
      expect { subject.send(:validate_ip) }.to raise_error(Error::InvalidInput)
    end

    it 'does not raise an error on a valid IP' do
      expect { valid_ip_instance.send(:validate_ip) }.not_to raise_error
    end

    it 'returns correct error message on an invalid IP' do
      expect { subject.send(:validate_ip) }.to raise_error('Line 1: /index has an invalid IP address: 126.318.035.038')
    end
  end
end
