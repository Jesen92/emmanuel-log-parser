# frozen_string_literal: true

require_relative '../lib/presenter/unique_view'

RSpec.describe Presenter::UniqueView do
  let(:views) {
    {
      '/help_page/1' => ['126.318.035.038', '929.398.951.889'],
      '/contact' => ['184.123.665.067', '184.123.665.067'],
      '/home' => ['184.123.665.067']
    }
  }

  context 'sorting' do
    it 'sorts views uniquely in descending order' do
      expect(described_class.sort(views)).to eq(
        [
          { page: '/help_page/1', visits: 2 },
          { page: '/contact', visits: 1 },
          { page: '/home', visits: 1 }
        ]
      )
    end
  end
end
