# frozen_string_literal: true

module Presenter
  class View
    def self.sort(views)
      views = views.sort_by { |_, visits| -visits.length }
      views.map { |page, visits| { page:, visits: visits.length } }
    end
  end
end
