# frozen_string_literal: true

module Presenter
  class UniqueView
    def self.sort(views)
      views = views.map { |page, visits| { page:, visits: visits.uniq.length } }
      views.sort_by { |page| -page[:visits] }
    end
  end
end
