# frozen_string_literal: true

module Presenter
  class Display
    class << self
      def print headers, rows
        puts generate_headers(headers) + generate_rows(headers, rows)
      end

      private

      LINE_WIDTH = 25

      def generate_headers headers
        line = ''
        last_header = headers.last
        headers.each { |header| line += header.equal?(last_header) ? header : header.ljust(LINE_WIDTH) }
        "#{line}\n"
      end

      def generate_rows headers, rows
        line = ''
        last_header = headers.last
        rows.each do |row|
          headers.each do |header|
            row_text = (row[:"#{header}"]).to_s
            line += header.equal?(last_header) ? row_text : row_text.ljust(LINE_WIDTH)
          end
          line += "\n"
        end
        line
      end
    end
  end
end
