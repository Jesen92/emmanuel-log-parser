# frozen_string_literal: true

require_relative '../error/invalid_input'

module Parser
  class File
    def initialize(file)
      @file = file
      @views = {}
      @pages = []
    end

    def call
      parse_file
      parse_pages

      views
    end

    private

    attr_accessor :pages, :views
    attr_reader :file

    def parse_file
      file.each_line.with_index do |line, index|
        data = line.split.map(&:strip)
        raise Error::InvalidInput, "page or view input missing on line #{index + 1}" if data.count < 2

        pages << Page.new({ page: data[0], view: data[1], line: index + 1 })
      end
    end

    def parse_pages
      pages.each do |page|
        views[page.page] = [] unless views[page.page]
        views[page.page] << page.view
      end
    end
  end
end
