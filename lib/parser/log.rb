# frozen_string_literal: true

require_relative '../parser/page'
require_relative '../parser/file'
require_relative '../presenter/display'
require_relative '../presenter/view'
require_relative '../presenter/unique_view'

require 'optparse'

module Parser
  class Log
    def initialize file
      @file = file[:file]
      @options = {}
    end

    def call
      file_values = File.new(file).call
      parse_options

      Presenter::Display.print(DISPLAY_HEADERS, Presenter::UniqueView.sort(file_values)) if options['u']
      Presenter::Display.print(DISPLAY_HEADERS, Presenter::View.sort(file_values)) unless options['u']

      file.close
    end

    private

    attr_reader :file, :options

    DISPLAY_HEADERS = %w[page visits].freeze

    def parse_options
      OptionParser.new do |opts|
        opts.on('-u', '--unique') { |u| options['u'] = u }
      end.parse!
    end
  end
end
