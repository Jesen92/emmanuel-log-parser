# frozen_string_literal: true

require 'resolv'
require_relative '../error/invalid_input'

module Parser
  class Page
    attr_reader :page, :view

    def initialize(data)
      @page = data[:page]
      @view = data[:view]
      @line = data[:line]

      # validate_ip
    end

    private

    attr_reader :line

    def validate_ip
      return if view =~ Resolv::IPv4::Regex

      raise Error::InvalidInput, "Line #{line}: #{page} has an invalid IP address: #{view}"
    end
  end
end
