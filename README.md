# Ruby log parser

This script will receive the **webserver.log** and do the following:
  1. list of webpages with most page views ordered from most page views to less page views
  2. list of webpages with most with most unique page views ordered from most page views to less page views

Feel free to make changes or design something if you think it meets the criteria above.
Don't feel pressured to add specs (tests). We will add them additionally and give you an intro to RSpec.
  
Few tips:
- write the script to be ready for expansions and additional features
- use **rubocop** for linting
- have in mind that Ruby is an object-oriented language and treat it as such.
- SOLID :)
