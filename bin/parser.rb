#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/parser/log'

FILE_PATH = ARGV[0].to_s

abort 'File is required' if FILE_PATH.nil? || FILE_PATH.strip.empty?
abort "#{FILE_PATH}: file extension is not `.log`" unless File.extname(FILE_PATH) == '.log'
abort "#{FILE_PATH}: no such file" unless File.file? FILE_PATH
abort "#{FILE_PATH}: file is empty" if File.zero? FILE_PATH

FILE = File.open(FILE_PATH, 'r')

Parser::Log.new({ file: FILE }).call
